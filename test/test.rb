require_relative 'spec_helper'

describe 'tcpdump test' do
  before(:all) do
    TCPDump.initialize
  end

  let(:version) do
    TCPDump.set_options(version: true)
    TCPDump.run
  end

  it 'should run tcpdump -h' do
    expect(version).to eq 'tcpdump -h'
  end
end
