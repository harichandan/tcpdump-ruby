# tcpdump-ruby
[![Gem Version](https://badge.fury.io/rb/tcpdump-ruby.svg)](https://badge.fury.io/rb/tcpdump-ruby)

A Ruby Wrapper around tcpdump

## Supported Options

      print_each_packet         ==>   -A
      print_asdot               ==>   -b
      set_buffer_size           ==>   -B
      exit_after_count          ==>   -c
      check_file_size           ==>   -C
      dump_human_readable       ==>   -d
      dump_c_program_fragment   ==>   -dd
      dump_decimal_numbers      ==>   -ddd
      list_interfaces           ==>   -D, --list-interfaces
      print_link_layer_header   ==>   -e
      print_ip_numeric          ==>   -f
      file_filter_expression    ==>   -F
      avoid_line_break          ==>   -g
      rotate_seconds            ==>   -G
      version                   ==>   -h, --help, --version
      interface                 ==>   -i, --interface
      monitor_mode              ==>   -I, --monitor-mode
      absolute_seq_num          ==>   -S, --absolute-tcp-sequence-numbers
      type                      ==>   -T

## Usage

Run the initialize method to initialize the default options. Then set the options using set_options method.

To set different options, you have to pass in hashes to the set_options method. For example, to run:

    tcpdump -h

you would set the version option to true.

    TCPDump.set_options(version: true)

To capture packets on eth0 interface, you would set the interface option to 'eth0'.

    TCPDump.set_options(interface: 'eth0')

You can pass in multiple key-value pairs to the set_options method. So, to run:

    tcpdump -i eth0 -S
    
you'd follow the following steps:

    TCPDump.initialize
    TCPDump.set_options(interface: 'eth0', absolute_seq_num: true)
    TCPDump.tcpdump
